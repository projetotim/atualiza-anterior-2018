DECLARE
	CURSOR c1 IS
		SELECT r.request_id
		  FROM kcrt_requests r
		       JOIN kcrt_request_types rt
		         ON rt.request_type_id = r.request_type_id
		            AND rt.reference_code = 'TIM_VALORACAO'
		 WHERE r.request_id IN ('221313');

	l_cod_categoria_servico VARCHAR2(200);
	l_nome_categoria_servico VARCHAR2(200);
BEGIN
	
	l_cod_categoria_servico := 'ANTERIOR_2018';
	l_nome_categoria_servico := 'Anterior a 2018';
	
	FOR c1_rec IN c1
	LOOP
		UPDATE kcrt_requests
		   SET last_update_date = sysdate,
		       last_updated_by = 1
		 WHERE request_id = c1_rec.request_id;

		UPDATE kcrt_request_details
		   SET last_update_date = sysdate,
		       last_updated_by = 1
		 WHERE request_id = c1_rec.request_id;

		UPDATE kcrt_req_header_details
		   SET last_update_date = sysdate,
		       last_updated_by = 1,
		       parameter23 = l_cod_categoria_servico,
		       visible_parameter23 = l_nome_categoria_servico
		 WHERE request_id = c1_rec.request_id;
	END LOOP;

END;
/